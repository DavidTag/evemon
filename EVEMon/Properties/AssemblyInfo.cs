using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("EVEMon")]
[assembly: AssemblyDescription("EVEMon Offline Character Utility. Revision Number: 4609 Repository URL: https://bitbucket.org/BattleClinic/evemon")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("EVEMon Development Team")]
[assembly: AssemblyProduct("EVEMon")]
[assembly: AssemblyCopyright("Copyright © 2006-2015, EVEMon Development Team")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Friend assembly for unit testing
[assembly: InternalsVisibleTo("Tests.EVEMon")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("dd0d5f0c-6de4-46d5-889e-0386ee699b13")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.9.4.4609")]

// Neutral Language
[assembly: NeutralResourcesLanguage("en-US")]
